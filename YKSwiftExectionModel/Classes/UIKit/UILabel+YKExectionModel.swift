//
//  UILabel+YKExectionModel.swift
//  YKSwiftExectionModel
//
//  Created by edward on 2021/11/18.
//

import UIKit

extension UILabel
{
    
    
    @objc public override func yk_x(_ x: CGFloat) -> UILabel {
        self.x = x
        return self
    }
    
    @objc public override func yk_y(_ y: CGFloat) -> UILabel {
        self.y = y
        return self
    }
    
    @objc public override func yk_maxX(_ maxX: CGFloat) -> UILabel {
        self.maxX = maxX;
        return self
    }
    
    @objc public override func yk_maxY(_ maxY: CGFloat) -> UILabel {
        self.maxY = maxY;
        return self
    }
    
    @objc public override func yk_centerX(_ centerX: CGFloat) -> UILabel {
        self.centerX = centerX
        return self
    }
    
    @objc public override func yk_centerY(_ centerY: CGFloat) -> UILabel {
        self.centerY = centerY
        return self
    }
    
    @objc public override func yk_width(_ width: CGFloat) -> UILabel {
        self.width = width
        return self
    }
    
    @objc public override func yk_height(_ height: CGFloat) -> UILabel {
        self.height = height
        return self
    }
    
    @objc public override func yk_backgroundColor(_ color: UIColor) -> UILabel {
        self.backgroundColor = color
        return self
    }
    
    @objc public override func yk_frame(_ frame: CGRect) -> UILabel {
        self.frame = frame
        return self
    }
    
    @objc public override func yk_raduis(_ raduis: CGFloat) -> UILabel {
        self.layer.cornerRadius = raduis
        return self
    }
    
    @objc public override func yk_borderColor(_ color: UIColor) -> UILabel {
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        return self
    }
}

extension UILabel
{
    public func yk_textColor(_ color: UIColor) -> UILabel {
        self.textColor = color
        return self
    }
    
    public func yk_text(_ text: String?) -> UILabel {
        self.text = text
        return self
    }
    
    public func yk_attrText(_ text: NSAttributedString?) -> UILabel {
        self.attributedText = text
        return self
    }
    
    public func yk_fontSize(size: CGFloat,weight: UIFont.Weight = .regular) -> UILabel {
        self.font = UIFont.systemFont(ofSize: size, weight: weight)
        return self
    }
    
    public func yk_fontSize(_ fontSize: CGFloat) -> UILabel {
        self.font = UIFont.systemFont(ofSize: fontSize)
        return self
    }
    
    public func yk_unlimitedLine() -> UILabel {
        self.numberOfLines = 0
        return self
    }
    
    public func yk_numberOfLine(_ number: Int) -> UILabel {
        self.numberOfLines = number
        return self
    }
    
    public func yk_textAlignment(_ alignment: NSTextAlignment) -> UILabel {
        self.textAlignment = alignment
        return self
    }
    
}
