//
//  UIImageView+YKExectionModel.swift
//  YKSwiftExectionModel
//
//  Created by edward on 2023/1/30.
//

import UIKit


extension UIImageView
{
    
    
    
    @objc public override func yk_x(_ x: CGFloat) -> UIImageView {
        self.x = x
        return self
    }
    
    @objc public override func yk_y(_ y: CGFloat) -> UIImageView {
        self.y = y
        return self
    }
    
    @objc public override func yk_maxX(_ maxX: CGFloat) -> UIImageView {
        self.maxX = maxX;
        return self
    }
    
    @objc public override func yk_maxY(_ maxY: CGFloat) -> UIImageView {
        self.maxY = maxY;
        return self
    }
    
    @objc public override func yk_centerX(_ centerX: CGFloat) -> UIImageView {
        self.centerX = centerX
        return self
    }
    
    @objc public override func yk_centerY(_ centerY: CGFloat) -> UIImageView {
        self.centerY = centerY
        return self
    }
    
    @objc public override func yk_width(_ width: CGFloat) -> UIImageView {
        self.width = width
        return self
    }
    
    @objc public override func yk_height(_ height: CGFloat) -> UIImageView {
        self.height = height
        return self
    }
    
    @objc public override func yk_backgroundColor(_ color: UIColor) -> UIImageView {
        self.backgroundColor = color
        return self
    }
    
    @objc public override func yk_frame(_ frame: CGRect) -> UIImageView {
        self.frame = frame
        return self
    }
    
    @objc public override func yk_raduis(_ raduis: CGFloat) -> UIImageView {
        self.layer.cornerRadius = raduis
        return self
    }
    
    @objc public override func yk_borderColor(_ color: UIColor) -> UIImageView {
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        return self
    }
}

extension UIImageView
{
    
}
