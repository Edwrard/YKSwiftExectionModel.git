//
//  UIView+FrameCore.swift
//  YKSwiftExectionModel
//
//  Created by edward on 2023/1/30.
//

import UIKit
 
extension UIView {
    
    
    
    public var centerX:CGFloat {
        set{
            var center = self.center
            center.x = newValue
            self.center = center
        }
        get{
            return self.center.x
        }
    }
    
    public var centerY:CGFloat {
        set{
            var center = self.center
            center.y = newValue
            self.center = center
        }
        get{
            return self.center.y
        }
    }
    
    public var size:CGSize {
        set{
            var farm = self.frame
            farm.size = newValue
            self.frame = farm
        }
        get{
            return self.frame.size
        }
    }
    
    public var x:CGFloat {
        set{
            var farm = self.frame
            farm.origin.x = newValue
            self.frame = farm
        }
        get{
            return self.frame.origin.x
        }
    }
    
    public var maxX:CGFloat {
        set{
            var frame = self.frame;
            frame.origin.x = newValue - frame.size.width;
            self.frame = frame;
        }
        get{
            return self.frame.maxX
        }
    }
    
    public var y:CGFloat {
        set{
            var farm = self.frame
            farm.origin.y = newValue
            self.frame = farm
        }
        get{
            return self.frame.origin.y
        }
    }
    
    public var maxY:CGFloat {
        set{
            var frame = self.frame;
            frame.origin.y = newValue - frame.size.height;
            self.frame = frame;
        }
        get{
            return self.frame.maxY
        }
    }
    
    public var width:CGFloat {
        set{
            var farm = self.frame
            farm.size.width = newValue
            self.frame = farm
        }
        get{
            return self.frame.size.width
        }
    }
    
    public var height:CGFloat {
        set{
            var farm = self.frame
            farm.size.height = newValue
            self.frame = farm
        }
        get{
            return self.frame.size.height
        }
    }
    
    
    
    
    @objc public func yk_x(_ x: CGFloat) -> UIView {
        self.x = x
        return self
    }
    
    @objc public func yk_y(_ y: CGFloat) -> UIView {
        self.y = y
        return self
    }
    
    @objc public func yk_maxX(_ maxX: CGFloat) -> UIView {
        self.maxX = maxX;
        return self
    }
    
    @objc public func yk_maxY(_ maxY: CGFloat) -> UIView {
        self.maxY = maxY;
        return self
    }
    
    @objc public func yk_centerX(_ centerX: CGFloat) -> UIView {
        self.centerX = centerX
        return self
    }
    
    @objc public func yk_centerY(_ centerY: CGFloat) -> UIView {
        self.centerY = centerY
        return self
    }
    
    @objc public func yk_width(_ width: CGFloat) -> UIView {
        self.width = width
        return self
    }
    
    @objc public func yk_height(_ height: CGFloat) -> UIView {
        self.height = height
        return self
    }
    
    @objc public func yk_backgroundColor(_ color: UIColor) -> UIView {
        self.backgroundColor = color
        return self
    }
    
    @objc public func yk_frame(_ frame: CGRect) -> UIView {
        self.frame = frame
        return self
    }
    
    @objc public func yk_raduis(_ raduis: CGFloat) -> UIView {
        self.layer.cornerRadius = raduis
        return self
    }
    
    @objc public func yk_borderColor(_ color: UIColor) -> UIView {
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        return self
    }
}
