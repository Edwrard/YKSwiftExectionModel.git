//
//  UIImage+Core.swift
//  YKSwiftExectionModel
//
//  Created by edward on 2021/5/20.
//

import Foundation
import UIKit


extension UIImage
{
    public static func yk_moduleImage(bundleName: String, className: String, imageNamed: String, imageType:String = "png") -> UIImage? {
        if ((imageNamed != "") && (bundleName != "") && (className != "")) {
            
            let bundle = Bundle(for: NSClassFromString(className) ?? NSObject.self)
            let url = bundle.url(forResource: bundleName, withExtension: "bundle")
            if url == nil {
                return nil
            }
            var imagename = ""
            
            let scale = UIScreen.main.scale
            if ((abs(scale - 3)) <= 0.001) {
                imagename = "\(imageNamed)@3x"
            }else if ((abs(scale - 2)) <= 0.01){
                imagename = "\(imageNamed)@2x"
            }else{
                imagename = imageNamed
            }
            
            var image:UIImage?
            if let imageBundle = Bundle.init(url: url!)
            {
                if let file = imageBundle.path(forResource: imagename, ofType: imageType) {
                    image = UIImage.init(contentsOfFile: file)
                }
                if image == nil,
                   let file = imageBundle.path(forResource: imageNamed, ofType: imageType) {
                    image = UIImage.init(contentsOfFile: file)
                }
                
                if image == nil {
                    image = UIImage.init(named: imagename)
                }
                
                if image == nil {
                    image = UIImage.init(named: imageNamed)
                }
                
                if image == nil {
                    image = UIImage.init(named: imagename, in: imageBundle, compatibleWith: nil)
                }
                
                if image == nil {
                    image = UIImage.init(named: imageNamed, in: imageBundle, compatibleWith: nil)
                }
            }
            
            return image
        }else{
            return nil
        }
    }
    
    
    @available(iOS 12.0, *)
    public func yk_compressToJPEGData(maxLength max:Int, eachQuality quality:CGFloat = 0.1) -> Data? {
        let resultImage = self
        var rate:CGFloat = 1
        
        #if swift(>=4.2)
        
        if var data = resultImage.jpegData(compressionQuality: rate) {
            while data.count > max {
                if let subData = resultImage.jpegData(compressionQuality: rate) {
                    data = subData
                }
                if quality < 1 && quality > 0 {
                    
                    rate = rate - quality
                }else {
                    
                    rate = rate - 0.1
                }
                if (rate <= 0) {
                    break;
                }
            }
            return data
        }else {
            return nil
        }
        #else
        
        return nil
        #endif
        
        
    }
}
