//
//  UIButton+YKExectionModel.swift
//  YKSwiftExectionModel
//
//  Created by edward on 2021/10/21.
//

import UIKit

extension UIButton
{
    
    
    
    @objc public override func yk_x(_ x: CGFloat) -> UIButton {
        self.x = x
        return self
    }
    
    @objc public override func yk_y(_ y: CGFloat) -> UIButton {
        self.y = y
        return self
    }
    
    @objc public override func yk_maxX(_ maxX: CGFloat) -> UIButton {
        self.maxX = maxX;
        return self
    }
    
    @objc public override func yk_maxY(_ maxY: CGFloat) -> UIButton {
        self.maxY = maxY;
        return self
    }
    
    @objc public override func yk_centerX(_ centerX: CGFloat) -> UIButton {
        self.centerX = centerX
        return self
    }
    
    @objc public override func yk_centerY(_ centerY: CGFloat) -> UIButton {
        self.centerY = centerY
        return self
    }
    
    @objc public override func yk_width(_ width: CGFloat) -> UIButton {
        self.width = width
        return self
    }
    
    @objc public override func yk_height(_ height: CGFloat) -> UIButton {
        self.height = height
        return self
    }
    
    @objc public override func yk_backgroundColor(_ color: UIColor) -> UIButton {
        self.backgroundColor = color
        return self
    }
    
    @objc public override func yk_frame(_ frame: CGRect) -> UIButton {
        self.frame = frame
        return self
    }
    
    @objc public override func yk_raduis(_ raduis: CGFloat) -> UIButton {
        self.layer.cornerRadius = raduis
        return self
    }
    
    @objc public override func yk_borderColor(_ color: UIColor) -> UIButton {
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        return self
    }
}

extension UIButton
{
    private struct AssociatedKey {
        static var eventKey: String = "eventKey"
    }
    
    @objc private func yk_eventAction(sender: UIButton) -> Void {
        guard let eventCallBack = objc_getAssociatedObject(self,&AssociatedKey.eventKey) as? ((_ sender:UIButton)->Void) else { return }
        eventCallBack(sender)
    }
    
//    public static func instance() -> UIButton {
//
//        return UIButton.init(type: .custom)
//    }
    
    
    public func yk_addEvent(event: UIControl.Event, eventCallBack:@escaping (_ sender:UIButton)->Void) -> Void {
        objc_setAssociatedObject(self,&AssociatedKey.eventKey,eventCallBack,.OBJC_ASSOCIATION_COPY_NONATOMIC)
        self.addTarget(self, action: #selector(yk_eventAction(sender:)), for: event)
        
    }
    
    public func yk_titleColor(color: UIColor, for state: UIControl.State = .normal) -> UIButton {
        self.setTitleColor(color, for: state)
        return self
    }
    
    
    public func yk_title(title: String, for state: UIControl.State = .normal) -> UIButton {
        self.setTitle(title, for: state)
        return self
    }
    
    public func yk_image(image: UIImage, for state: UIControl.State = .normal) -> UIButton {
        self.setImage(image, for: state)
        return self
    }
    
    public func yk_backgroundImage(image: UIImage, for state: UIControl.State = .normal) -> UIButton {
        self.setBackgroundImage(image, for: state)
        return self
    }
    
    public func yk_titleInset(_ titleInset: UIEdgeInsets) -> UIButton {
        self.titleEdgeInsets = titleInset
        return self
    }
    
    public func yk_imageInset(_ imageInset: UIEdgeInsets) -> UIButton {
        self.imageEdgeInsets = imageInset
        return self
    }
    
    public func yk_fontSize(size:CGFloat, weight:UIFont.Weight = .medium) -> UIButton {
        self.titleLabel?.font = UIFont.systemFont(ofSize: size, weight: weight)
        return self
    }
}
