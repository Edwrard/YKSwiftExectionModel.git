//
//  Array+YKExectionModel.swift
//  YKSwiftExectionModel
//
//  Created by edward on 2021/11/19.
//

import Foundation

public extension Array {
    
    
    func enumerateObjectsUsingBlock(enumerBlock:(_ obj:Element,_ index:Int)->Void) -> Void {
        for (i,item) in self.enumerated() {
            enumerBlock(item,i)
        }
    }
    
    func safeGetObject(index:Int) -> Element? {
        return (0 ..< count).contains(index) ? self[index] : nil
    }
    
    subscript (safe index: Int) -> Element? {
        return (0 ..< count).contains(index) ? self[index] : nil
    }
    
    subscript (arrayKey index:Int) -> [Any] {
        if let value = self[safe: index] as? [Any] {
            return value
        }else {
            return []
        }
    }
    
    func map(arrayMap:((_ obj:Element) -> Element)) -> [Element] {
        
        var a:[Element] = []
        
        self.enumerateObjectsUsingBlock { obj, index in
            a.append(arrayMap(obj))
        }
        
        return a
    }
    
    func filter(arrayFilter:((_ obj:Element, _ idx:Int) -> Bool)) -> [Element] {
        var a:[Element] = []
        
        self.enumerateObjectsUsingBlock { obj, index in
            if arrayFilter(obj,index) {
                a.append(obj)
            }
        }
        
        return a
    }
    
    func yk_arrayJson() -> String {
        let stringData = try! JSONSerialization.data(withJSONObject: self, options: [])
        if let str = String(data: stringData, encoding: .utf8)
        {
            return str
        }else {
            return ""
        }
    }
}
