# YKSwiftExectionModel

[![CI Status](https://img.shields.io/travis/edward/YKSwiftExectionModel.svg?style=flat)](https://travis-ci.org/edward/YKSwiftExectionModel)
[![Version](https://img.shields.io/cocoapods/v/YKSwiftExectionModel.svg?style=flat)](https://cocoapods.org/pods/YKSwiftExectionModel)
[![License](https://img.shields.io/cocoapods/l/YKSwiftExectionModel.svg?style=flat)](https://cocoapods.org/pods/YKSwiftExectionModel)
[![Platform](https://img.shields.io/cocoapods/p/YKSwiftExectionModel.svg?style=flat)](https://cocoapods.org/pods/YKSwiftExectionModel)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YKSwiftExectionModel is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YKSwiftExectionModel'
```

## Author

edward, 534272374@qq.com

## License

YKSwiftExectionModel is available under the MIT license. See the LICENSE file for more info.
